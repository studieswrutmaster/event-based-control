figure
plot(ScopeData1.time,ScopeData1.signals(1).values)
grid on
xlabel('time [s]')
ylabel('states')
title('States')
legend('robot','lights')
axis([0 ScopeData1.time(end) -0.1 4.1])
saveas(gcf,'states','epsc')


figure
plot(ScopeData1.time,ScopeData1.signals(2).values)
grid on
xlabel('time [s]')
ylabel('distance [m]')
title('Distances')
axis([0 ScopeData1.time(end) -0.1 500.1])
saveas(gcf,'distance','epsc')


figure
plot(ScopeData1.time,ScopeData1.signals(3).values)
grid on
xlabel('time [s]')
ylabel('velocity [m/s]')
title('Velocities')
axis([0 ScopeData1.time(end) -0.1 1.1])
saveas(gcf,'velocity','epsc')
