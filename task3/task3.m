clear;clc;
close all;
%% time simulation
T = 10;
Tsim = 20;

%% initial velocities
vr = 2.5;
vg = 2.5;
vb = 3;


%% initial contitions for robots
w = 2*pi/T;
Red0 = [3*w,-w,-3,0,pi/2];
Green0 = [2.5*w,-w,8.5,0,-pi/2];
Blue0 = [4.5*w,w,4.5,-4.5,0];

%% simulation
sim('task3sim',Tsim)

%% visualization
for i = 1:length(tout)
    fig = figure('visible','off');
%     figure(1)
    plot(Red(1:i,1),Red(1:i,2),'r',...
        Green(1:i,1),Green(1:i,2),'g',...
        Blue(1:i,1),Blue(1:i,2),'b',...
        Red(i,1),Red(i,2),'ro',...
        Green(i,1),Green(i,2),'go',...
        Blue(i,1),Blue(i,2),'bo','markers',15)
%     if i > tail
%         plot(Red(i-tail:i,1),Red(i-tail:i,2), ...
%         'r',Green(i-tail:i,1),Green(i-tail:i,2),'g',...
%         Blue(i-tail:i,1),Blue(i-tail:i,2),'b',...
%         Red(i,1),Red(i,2),'ro',...
%         Green(i,1),Green(i,2),'go',...
%         Blue(i,1),Blue(i,2),'bo','markers',15)
%     end
    grid on
    axis([-4 10 -6 6])
    xlabel('x [m]')
    ylabel('y [m]')
%     figure(2)
%     plot(tout(1:i),system_state_controller(1:i,:));
%     xlabel('time [s]')
%     ylabel('state')
%     title('system state')
%     axis([0 Tsim 0.9 6.1])
%     figure(4)
%     plot(tout(1:i),system_state(1:i,:));
%     xlabel('time [s]')
%     ylabel('state')
%     title('system state')
%     axis([0 Tsim 0.9 6.1])
%     figure(5)
%     plot(tout(1:i),next_state(1:i,:));
%     xlabel('time [s]')
%     ylabel('state')
%     title('next system state')
%     axis([0 Tsim 0.9 6.1])
%     figure(3)
%     plot(tout(1:i),controlable_events(1:i,:));
%     xlabel('time [s]')
%     ylabel('event')
%     title('controlable events (robot get permitions)')
%     axis([0 Tsim -0.1 1.1])
%     figure(4)
%     plot(tout(1:i),observable_events(1:i,:));
%     xlabel('time [s]')
%     ylabel('event')
%     title('observable events (robot letf sector)')
%     axis([0 Tsim -0.1 1.1])
      name = sprintf('xy%d',i);
      saveas(fig,name,'png');
%     pause(0.05)
end


% bounds = [150,-150;
%     125,150;
%     45,125;
%     -45,45;
%     -125,-45;
%     -150,-125];
% figure(1)
% hold on
% R = 4.5;
% for i = 1:length(bounds(:,2))
%     plot(R*cos(bounds(i,2)*pi/180)+4.5,R*sin(bounds(i,2)*pi/180),'ro')
% end
% hold off