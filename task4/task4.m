clc;clear all;close all

%% system definition -- algebraic representation of Petri Net

     %t1,t2,t3,t4
Xm = [[1,0,0,0];%p0
      [0,1,0,0];%p1
      [0,0,1,0];%p2
      [0,0,0,1];%p3
      [0,0,0,0];%p4
      [1,0,0,0];%M1
      [0,1,0,0];%M2
      [0,0,1,0]];%M3

     %t1,t2,t3,t4
Xp = [[0,0,0,0];%p0
      [1,0,0,0];%p1
      [0,1,0,0];%p2
      [0,0,1,0];%p3
      [0,0,0,1];%p4
      [0,1,0,0];%M1
      [0,0,1,0];%M2
      [0,0,0,1]];%M3  
  


%% general parameters
Tsim = 16; %s
no_of_processes = 3;
no_of_machines = 3;
process_time_execution = 1;%s

%% system parameters
init_state = [no_of_processes;0;0;0;0;1;1;1];
init_controlable_event = [0;0;0;0];
X = Xp - Xm;  
%% simulation

sim('task4sim',Tsim)

%% results
% results = zeros(length(tout),no_of_machines);

delay = process_time_execution/0.05;
results = kron(observable_events(:,1:no_of_machines),ones(delay,1));
results = results(1:length(tout),:);

processes = zeros(length(tout),no_of_processes);
j=1;
for i = 1:length(tout)
    if(observable_events(i,j) == 1)
        if (i > 1)&&(observable_events(i,4)<no_of_processes)
            processes(i,observable_events(i,4)+1) = processes(i-1,observable_events(i,4)+1) + 1;
        end
        j = j+1;
        if j == 4
            j = 1;
        end    
    end

end
processes = kron(processes(:,1:no_of_machines),ones(delay,1));
processes = processes(1:length(tout),:);


figure(1)
plot(tout,results)
axis([0 Tsim -0.1 1.1])
legend('m_1','m_2','m_3')
xlabel('time [s]')
ylabel('1- occupy, 0 - free')

figure(2)
plot(tout,processes)
axis([0 Tsim -0.1 3.1])
legend('proc1','proc2','proc3')
xlabel('time [s]')
ylabel('number of the machine')